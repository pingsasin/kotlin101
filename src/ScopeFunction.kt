data class User(val username: String, val lastLoggedIn: Long, val isBanned: Boolean) {
    var favoriteFood: String = ""
    var favoriteColor: String = ""
}

fun main(args: Array<String>) {
    val someOne = User("abcdefgh", 454345L, false)
    someOne.favoriteColor = "Red"
    someOne.favoriteFood = "Banana"

    val noOne = someOne.copy(username = "ping")

    println("$someOne, ${someOne.favoriteFood}, ${someOne.favoriteColor}")
    println("$noOne, ${noOne.favoriteColor}")
}