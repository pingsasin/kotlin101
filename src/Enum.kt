import java.util.*


// Basic enum class
enum class Vehicle {
    TRAIN, PLANE, SHIP
}

// Initialization enum class
enum class Country(val locale: Locale) {
    THAI(Locale("th", "TH")),
    UNITED_KINGDOM(Locale.UK),
    CHINA(Locale.CHINA),
    FRANCE(Locale.FRANCE)
}

// Anonymous enum class
enum class Transportation {
    AIR {
        override fun by(): Vehicle = Vehicle.PLANE
    },
    GROUND {
        override fun by(): Vehicle = Vehicle.TRAIN
    },
    OCEAN {
        override fun by(): Vehicle = Vehicle.SHIP
    };

    abstract fun by(): Vehicle
}

//Every enum constant has properties to obtain its name and position in the enum class declaration: val name: String , val ordinal: Int


fun main(args: Array<String>) {

    printAllValues<Transportation>()

}

inline fun <reified T : Enum<T>> printAllValues() {
    print(enumValues<T>().joinToString { it.name })

}