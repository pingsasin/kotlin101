// mutableList don't care about implementation. behavior is same as arrayList.
val footballClubMutableList = mutableListOf(
        "Arsenal",
        "Bournemouth",
        "Brighton and Hove Albion",
        "Burnley",
        "Cardiff City",
        "Chelsea",
        "Crystal Palace",
        "Everton",
        "Fulham",
        "Huddersfield Town",
        "Leicester City",
        "Liverpool",
        "Manchester City",
        "Manchester United",
        "Newcastle United",
        "Southampton",
        "Tottenham Hotspur",
        "Watford",
        "West Ham United",
        "Wolverhampton Wanderers")


// specific to be array list.
val footballClubArrayList = arrayListOf(
        "Arsenal",
        "Bournemouth",
        "Brighton and Hove Albion",
        "Burnley",
        "Cardiff City",
        "Chelsea",
        "Crystal Palace",
        "Everton",
        "Fulham",
        "Huddersfield Town",
        "Leicester City",
        "Liverpool",
        "Manchester City",
        "Manchester United",
        "Newcastle United",
        "Southampton",
        "Tottenham Hotspur",
        "Watford",
        "West Ham United",
        "Wolverhampton Wanderers")


// read only
val footballClubList = listOf(
        "Arsenal",
        "Bournemouth",
        "Brighton and Hove Albion",
        "Burnley",
        "Cardiff City",
        "Chelsea",
        "Crystal Palace",
        "Everton",
        "Fulham",
        "Huddersfield Town",
        "Leicester City",
        "Liverpool",
        "Manchester City",
        "Manchester United",
        "Newcastle United",
        "Southampton",
        "Tottenham Hotspur",
        "Watford",
        "West Ham United",
        "Wolverhampton Wanderers")


val footBallClubArray = arrayOf("Man city", "Liverpool", "Spurs", "Chelsea")


val footballClubMap = mapOf(Pair("liverpool", "Premier League"), ("Barcelona" to "la Liga"))

val emptyList = mutableListOf<String>()


val groupA = arrayOf("Jane", "Marry", "Alice")

val groupTourWithoutStar = mutableListOf<Any>("Peter", "Will", "John", groupA)
val groupTourWithStar = mutableListOf<Any>("Peter", "Will", "John", *groupA)


fun main(args: Array<String>) {


    groupTourWithStar.forEach {
        println("$it")
    }

    groupTourWithoutStar.forEach{
        println("$it")
    }


    if (footballClubMutableList.none { it == "Real madrid" }) println("no real madrid in list.")

    val firstTeam = footballClubList.first()
    val lastTeam = footballClubList.last()

    println("first team is $firstTeam, last team is $lastTeam")


}
