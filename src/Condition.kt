fun main(args: Array<String>) {

//    ifElseSample()

    whenSimple()


}

fun whenSimple() {
    val x = 100
    when (x) {
        in 1..30 -> {

        }

        !in 80..110 -> {

        }

        in 200 downTo 150 -> {
        }
    }
}

fun describe(obj: Any): String =
        when (obj) {
            1 -> "One"
            "Hello" -> "Greeting"
            is Long -> "Long"
            !is String -> "Not a string"
            else -> "Unknown"
        }

fun ifElseSample() {

    var isAllow = false

    if (isAllow) {
        println("continue.")
    } else {
        println("not allow.")

    }


    if (isAllow) println("continue.") else println("not allow.")

    println(if (isAllow) "continue" else "not allow.")
}