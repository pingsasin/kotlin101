// Numbers

private val double: Double = 100.0
private val float: Float = 100.0f
private val long: Long = 100L // for Longs are tagged by a capital L
private val mInt: Int = 100
private val mShort: Short = 100
private val mByte: Byte = 100

// hexadecimal [start with 0x]
private val hexa: Int = 0x0F_0B

// binary [start with 0b]
private val binary = 0b00001011_00001011_10010100


// return Unit
fun helloWorld() {
    println("hello world")
}

fun sum(a: Number, b: Number): Int {
    return a.toInt() + b.toInt()
}

fun hello(name: String): String {
    return "Hello, $name"
}

// normally kotlin can use smart case for return type.

fun hiWorld() = println("hi world")

fun diff(x: Number, y: Number): Int = x.toInt() - y.toInt()

fun hi(name: String): String = "hi $name"

fun main(args: Array<String>) {



}


