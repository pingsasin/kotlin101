import kotlin.math.absoluteValue

fun main(args: Array<String>) {
    breakAndContinue()
}


fun forLoop() {
    footballClubMutableList.forEach { print("$it, ") }

    for (team in footballClubMutableList) {
        println("$team, ")
    }

    for (teamIndex in footballClubMutableList.indices) {
        println("$teamIndex, ")
    }


    for ((index, team) in footballClubMutableList.withIndex()) {
        println("$index:$team, ")
    }

}

fun breakAndContinue() {

    var firstCount = 0
    var secondCount = 0


    loop@ for (first in 1..1000) {
        for (second in 100..400) {
            firstCount++
            if (first + second == 500) break@loop
        }
    }


    for (first in 1..1000) {
        for (second in 100..400) {
            secondCount++
            if (first + second == 500) {
                break
            }
        }
    }
    println("firstCount is $firstCount")
    print("secondCount is $secondCount")
}


fun anotherLoop() {
    for (number in 0..100 step 3) {
        println(number)
    }

    for (same in 0 until 100 step 3) {
        println(same)
    }


    for (i in 0..footballClubArrayList.size - 1) {
    }  // bad
    for (i in 0 until footballClubArrayList.size) {
    }  // good
    for (backward in 1000 downTo 1 step 5) {
        println(backward)
    }
}

































