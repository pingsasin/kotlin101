/* 
Copyright (c) 2019 Kotlin Data Classes Generated from JSON powered by http://www.json2kotlin.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */
package moshi.model


data class Clubs (

	val id : Int,
	val name : String,
	val short_name : String,
	val club_code : String,
	val stadium : String,
	val capacity : Int,
	val manager : String,
	val manager_nationality : String,
	val position : Int,
	val last_week_position : Int,
	val games_played : Int,
	val home_games_played : Int,
	val away_games_played : Int,
	val wins : Int,
	val home_wins : Int,
	val away_wins : Int,
	val draws : Int,
	val home_draws : Int,
	val away_draws : Int,
	val losses : Int,
	val home_losses : Int,
	val away_losses : Int,
	val goals_scored : Int,
	val home_goals_scored : Int,
	val away_goals_scored : Int,
	val goals_conceded : Int,
	val home_goals_conceded : Int,
	val away_goals_conceded : Int,
	val goals_difference : Int,
	val points : Int,
	val home_points : Int,
	val away_points : Int,
	val next_fixture : String,
	val previous_fixtures : List<String>,
	val results : List<String>,
	val form : List<String>
)