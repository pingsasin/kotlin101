package moshi

import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import moshi.model.Players
import java.util.*

object MoshiBuilder {
    val moshiInstance: Moshi
        get() = Moshi.Builder()
                .add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
                .add(DefaultOnDataMismatchAdapter.newFactory(Players::class.java, null))
                .add(FilterNullValuesFromListAdapter.newFactory(Players::class.java))
                .build()
}